const rules1 = require("./rules/01-possible-errors");
const rules2 = require("./rules/02-best-practices");
const rules3 = require("./rules/03-strict");
const rules4 = require("./rules/04-variables");
const rules5 = require("./rules/05-nodejs-and-commonjs");
const rules6 = require("./rules/06-stylistic-issues");
const rules7 = require("./rules/07-ecmascript-6");

module.exports = {
  "env": {
    "es6": true,
    "node": true,
  },
  "parserOptions": {
    "ecmaVersion": 2018,
    "sourceType": "module",
  },
  "rules": {
    ...rules1,
    ...rules2,
    ...rules3,
    ...rules4,
    ...rules5,
    ...rules6,
    ...rules7,
  },
};
